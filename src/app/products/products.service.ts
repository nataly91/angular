import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
  //private _url = 'http://jsonplaceholder.typicode.com/products';

  productsObservable;


  getProducts(){
    //return this._http.get(this._url).map(res =>res.json()).delay(2000)
   /* this.productsObservable = this.af.database.list('/products');
    return this.productsObservable; */

    this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.categoryName = [];
            for(var c in product.categoryId){
                product.categoryName.push(
                this.af.database.object('/categorys/' + c)
              )
            }
          }
        );
        return products;
      }
    )
    
    return this.productsObservable;
   
  }

  deleteProduct(product){
    let productKey = product.$key;
    this.af.database.object('/products/' + productKey).remove();
  }
  
  
  constructor(private af:AngularFire) { }

}