import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Product} from './product';

@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs: ['product']
})
export class ProductComponent implements OnInit {

  product:Product;
  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() editEvent = new EventEmitter<Product>();
  isEdit:Boolean = false;
  editButtonText = "Edit";
  
  
  constructor() { }

sendDelete(){
  this.deleteEvent.emit(this.product);
}
  
toggleEdit(){
  this.isEdit = !this.isEdit;
  this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
   if(!this.isEdit){
    this.editEvent.emit(this.product);
  }
}

  ngOnInit() {
  }

}