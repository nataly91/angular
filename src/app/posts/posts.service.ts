import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class PostsService {
 // private _url = 'http://jsonplaceholder.typicode.com/posts';

 postsObservable;

  getPosts(){
     //return this._http.get(this._url).map(res =>res.json()).delay(2000)
     //this.postsObservable = this.af.database.list('/posts');
    this.postsObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.useName = [];
            for(var u in post.users){
                post.useName.push(
                this.af.database.object('/users/' + u)
              )
            }
          }
        );
        return posts;
      }
    )
    
    return this.postsObservable;
 
  }

  addPost(post){
    this.postsObservable.push(post);
  }

  updatePost(post) {
    let postKey = post.$key;
    let postData = {title:post.title, body:post.body, author:post.author};
    this.af.database.object('/posts/' + postKey).update(postData);
  }

  deletePost(post){
    let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
  }
  
  constructor(private af:AngularFire) { }

}