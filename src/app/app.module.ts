import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import {PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UserComponent } from './user/user.component';
import { FireComponent } from './fire/fire.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService} from './products/products.service';


export const firebaseConfig = {
  apiKey: "AIzaSyBPB3f1iO-0oem2SLbE-yPrpps5FDSlenM",
    authDomain: "angular-9564b.firebaseapp.com",
    databaseURL: "https://angular-9564b.firebaseio.com",
    storageBucket: "angular-9564b.appspot.com",
    messagingSenderId: "425007320957"
}

const appRoutes:Routes = [
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path: 'fire', component:FireComponent},
  {path: 'products', component:ProductsComponent},
  {path:'', component:UsersComponent},
  {path:'', component:PostsComponent},
  {path: '', component:ProductsComponent},
  {path:'**', component:PageNotFoundComponent},

]


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    UserComponent,
    FireComponent,
    ProductComponent,
    ProductsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService, UsersService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
